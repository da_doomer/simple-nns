#!/usr/bin/env python3

from setuptools import setup

setup(
    name='simple_nns',
    version='0.1',
    install_requires=['numpy'],
    description='Simple neural networks',
    packages=['simple_nns', 'simple_nns.networks'],
    author='da_doomer',
    license='MIT',
    platforms='Linux; Windows; OS X'
    )
