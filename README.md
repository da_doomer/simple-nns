# simple-nns

*Because you don't always need autograd.*

Neural network semantics in an easily serializable, tiny implementation. The
only dependency is Numpy, used for fast forward-semantics.

## Installation

Install with your package manager of choice. With the standard `pip` this is:

```bash
python -m pip install git+https://gitlab.com/da_doomer/simple-nns.git
```

## Use

```python
import pickle
import random
from simple_nns.networks import MLP
import numpy as np

# Instantiate a multi layer perceptron with randomly initialized weights
f = MLP.from_uniform(ins=4, outs=4, inner_sizes=[32, 32], activation="relu")

# Fast forward semantics are implemented with numpy
x = [random.random() for _ in range(4)]
y = f(x)
assert len(y) == 4

# Serialization is trivial, as all objects are JSON serializable
with open("mlp.json", "wt") as fp:
    fp.write(f.json)

with open("mlp.json", "rt") as fp:
    f2 = MLP.from_json(list(fp)[0])

assert f == f2
assert f(x) == f2(x)

# Networks are hashable
set_of_mlps = set([f, f2])
assert len(set_of_mlps) == 1

# Networks can be easily pickled
pickle.dumps(f)

# Modify weights by creating a new network
new_layers = list()
for W, b in f.layers:
    W2 = W + np.random.normal(size=np.array(W).shape)
    b2 = b + np.random.normal(size=np.array(b).shape)
    new_layers.append((W2, b2))
f3 = MLP(new_layers, f.activation)

assert f(x) != f3(x)
```
