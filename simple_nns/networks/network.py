""" Interface for neural networks."""
# Native imports
from abc import ABC
from abc import abstractmethod
from typing import List


class Network(ABC):
    """A generic Network."""
    @property
    @abstractmethod
    def json(self) -> str:
        """The JSON string representation of a Network."""

    @staticmethod
    @abstractmethod
    def from_json(json_: str) -> "Network":
        """De-serialize the given serialized Network."""

    @abstractmethod
    def __call__(self, x):
        """Forward semantics for the given input."""
