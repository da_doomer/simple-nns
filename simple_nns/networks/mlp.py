"""The classic multi-layer perceptron: a sequence of functions of the form

    f_i(x) = g(W_i x + b_i)

."""
# Native imports
from dataclasses import dataclass
from dataclasses import asdict
import functools
import json

# External imports
import numpy as np
from numpy.random import default_rng

# Local imports
from .network import Network


Matrix = tuple[tuple[float, ...], ...]
Vector = tuple[float, ...]
Layer = tuple[Matrix, Vector]


def _activated(x, activation: str):
    """Return x under the given activation function."""
    if activation == "tanh":
        return np.tanh(x)
    if activation == "relu":
        return x.clip(min=0.0)
    raise ValueError(f"Invalid activation {activation}.")


@functools.cache
def _numpy_layers(mlp: "MLP") -> list[tuple[np.ndarray, np.ndarray]]:
    """Return the layers of the MLP as tuples of numpy arrays."""
    layers = list()
    for w_parameters, b_parameters in mlp.layers:
        layers.append((np.array(w_parameters), np.array(b_parameters)))
    return layers


def _numpy_forward(mlp: "MLP", x: np.ndarray) -> np.ndarray:
    """Return the layers of the MLP as tuples of numpy arrays."""
    layers = _numpy_layers(mlp)
    for i, (W, b) in enumerate(layers):
        x = W.T@x + b
        if i < len(layers)-1:
            x = _activated(x, mlp.activation)
    return x


@dataclass(frozen=True, eq=True)
class MLP(Network):
    """The classic multi-layer perceptron: a sequence of functions of the form

        f_i(x) = g(W_i x + b_i).

       The available activation functions are: ["relu", "tanh"].

       Parameters are initialized with a standard normal distribution with
       mean zero and standard deviation of 1/max(*inner_sizes, ins, outs)
    """
    layers: tuple[Layer, ...]
    activation: str

    def __post_init__(self):
        # Instead of expecting the user to provide hashable layers, we convert
        # them to hashable tuples post-initialization.
        tuple_layers: tuple[Layer, ...] = tuple()
        for w_parameters, b_parameters in self.layers:
            w = tuple(tuple(row) for row in w_parameters)
            b = tuple(b_parameters)
            tuple_layers = tuple_layers + ((w, b),)
        object.__setattr__(self, "layers", tuple_layers)

    @staticmethod
    def from_uniform(
            ins: int,
            outs: int,
            inner_sizes: list[int],
            **kwargs,
            ):
        """Uniform initialization for each layer."""
        layer_sizes = [ins] + inner_sizes + [outs]
        w_sizes = [
            (layer_sizes[i], layer_sizes[i+1])
            for i in range(len(layer_sizes)-1)
        ]
        b_sizes = layer_sizes[1:]
        layers = list()
        for w_size, b_size in zip(w_sizes, b_sizes):
            rng = default_rng()
            stdev = 1/np.sqrt(w_size[0])
            w_parameters = rng.uniform(size=(w_size))*stdev
            b_parameters = rng.uniform(size=(b_size,))*stdev
            layers.append((w_parameters, b_parameters))
        return MLP(layers, **kwargs)

    @property
    def ins(self) -> int:
        """Number of input neurons."""
        return len(self.layers[0][0])

    @property
    def outs(self) -> int:
        """Number of output neurons."""
        return len(self.layers[-1][1])

    @property
    def inner_sizes(self) -> list[int]:
        """Size of each hidden layer."""
        return [len(layer[1]) for layer in self.layers[:-1]]

    @property
    def json(self) -> str:
        """The JSON string representation of a Network."""
        return json.dumps(
                asdict(self),
                indent=4,
                )

    @staticmethod
    def from_json(json_: str) -> Network:
        """De-serialize the given serialized Network."""
        return MLP(**json.loads(json_))

    def __call__(self, x: list[float]) -> list[float]:
        """The input is a single (non-batched) vector."""
        return _numpy_forward(self, np.array(x)).tolist()
